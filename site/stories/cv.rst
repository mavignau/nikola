==============================
Maria Andrea Vignau
==============================

.. figure:: /galleries/mavignau.jpg
   :width: 500px
   :align: center
   :figwidth: 60%
   :alt: Maria Andrea Vignau

Senior Backend Python Developer | International Speaker

Contact Information
------------------------------
- **Email:** mavignau@gmail.com
- **LinkedIn:** [linkedin.com/in/mavignau](https://www.linkedin.com/in/mavignau/)
- **GitHub:** [github.com/marian-vignau](https://github.com/marian-vignau)
- **GitLab:** [gitlab.com/mavignau](https://gitlab.com/mavignau)
- **Location:** Resistencia, Chaco, Argentina

Professional Summary
------------------------------
Maria Andrea Vignau is a seasoned Backend Python Developer with over two decades of experience in software engineering. She has excelled in developing high-performance backend systems, mentoring aspiring developers, and contributing to the open-source community. An engaging international speaker, Maria has delivered impactful talks across Argentina, Bolivia, Europe, and the USA, sharing her expertise in Python and backend technologies.

Technical Skills
------------------------------
- **Languages:** Python (20+ years), SQL, JavaScript
- **Frameworks and Tools:** Flask, FastAPI, Django, SQLAlchemy, PyTest, Poetry, Docker
- **Databases:** MySQL, PostgreSQL, MongoDB, Redis
- **Cloud Technologies:** AWS (Lambda, S3, EC2), Datadog, Jenkins
- **Development Practices:** RESTful APIs, GraphQL, OAuth, Microservices
- **Additional Expertise:** Linux administration, Jupyter Notebooks, Forensic Analysis Tools

Certifications
------------------------------
- IBM Full Stack Software Developer Specialization
- Developing AI Applications with Python and Flask
- Generative AI: Introduction and Applications
- Django Application Development with SQL and Databases
- Mastery in Linux Command Line Tools

Professional Experience
------------------------------
**Turing**  
Senior Python Backend Developer  
May 2023 – February 2024  
- Developed and optimized microservices on AWS Lambda.
- Implemented Datadog monitoring and CI/CD pipelines using GitLab Actions.
- Delivered robust RESTful APIs to enhance system integrations.

**Bonzuu**  
Senior Backend Developer  
November 2023 – April 2024  
- Enhanced database performance through optimized SQL queries.
- Developed FastAPI-based services and ensured high test coverage.
- Maintained and improved backend systems with PyTest and Jenkins.

**ShipHero**  
Backend Developer  
July 2021 – January 2023  
- Designed and maintained APIs integrating with FedEx and other external systems.
- Leveraged AWS S3, AuroraDB, and Lambda for scalable backend solutions.
- Acted as a key problem solver for production issues.

**Judicial Branch of Chaco**  
Computer Forensic Expert  
May 2016 – February 2022  
- Conducted forensic investigations using Autopsy and Cellebrite UFED.
- Delivered expert witness testimony in high-profile cases.

Public Speaking & Community Contributions
--------------------------------------------
Maria has passionately contributed to the tech community as a speaker and organizer:

**Talks Delivered:**
~~~~~~~~~~~~~~~~~~~~~~~~
* "Transform Your Data in Gold with SQLAlchemy II" (PyDay NEA, PyCon US, EuroPython)
* "Dance with Shadows: Stubs, Patch, and Mock" (EuroPython, PyCon US)
* "Cellphone: A Quiet Witness" (Nerdearla, NotPinkConf)

**Events Organized:**
~~~~~~~~~~~~~~~~~~~~~~~~
* PyDay NEA (2022, 2023)
* AguaraTech (2023)
* FLISOL (Multiple Years)

Education
------------------------------
- **Information Systems Engineer**
  Universidad Tecnológica Nacional, Resistencia, Argentina
- **Diploma in Judicial Expertise**
  Austral University

Languages
------------------------------
- **Spanish:** Native
- **English:** C1 (Fluent Conversational)

Achievements
------------------------------
- Authored open-source tools such as CertMailer and DecodeOLX.
- Regular contributor to Python community projects and conferences.

Maria Andrea Vignau’s passion for innovation, leadership, and education makes her an invaluable asset in any backend development or technical advocacy role.


