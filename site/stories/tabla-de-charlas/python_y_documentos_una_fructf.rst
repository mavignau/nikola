.. title: Python y Documentos: una fructífera relación
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
¿Quien no peleó para generar, modificar o leer PDF? 
Un breve repaso de las opciones que existen, se usan y nos resuelven la vida.

Contenido
=========
- Generar 
  1. HTML
  2. ReSTructured Text, Markdown
  3. Formularios PDF
  4. LibreOfficeWriter y SVG
- Leer
- Modificar

Recursos
=============
🎞  `Video Meetup Python Argentina Febrero <https://youtu.be/5uISrui3USI>`_

💾: `Diapositivas Python y documentos </Python%20y%20documentos.pdf>`_



Bibliotecas mencionadas
~~~~~~~~~~~~~~~~~~~~~~~
- `weasyprint <https://doc.courtbouillon.org/weasyprint/stable/index.html>`_
- `xhtml2pdf <https://xhtml2pdf.readthedocs.io/en/latest/index.html>`_
- `rinohtype <http://www.mos6581.org/rinohtype/master>`_
- `secretary <https://github.com/christopher-ramirez/secretary>`_
- `rst2pdf <https://rst2pdf.org>`_
- `pdfminer.six <https://pdfminersix.readthedocs.io/en/latest/index.html>`_
- `Camelot <https://camelot-py.readthedocs.io/en/master/index.html>`_
- `PyMuPDF <https://pymupdf.readthedocs.io/en/latest/index.html>`_
- `PyPDF3 <https://pythonhosted.org/PyPDF2>`_

