.. title: Introducción a la computación científica con Python
.. date: 2023-10-07 23:22:34 UTC+03:00
.. tags: taller historial refencias
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
Una de las áreas con mayor crecimiento es el análisis de datos, base fundamental para la revolución de la inteligencia artificial. Jupyter notebook, Pandas, Numpy, Matplotlib son las librerías básicas y mostraré brevemente su funcionamiento

Objetivo
========
Dar una introducción a las herramientas de informática científica usando Python.

Descripción
===============
Una de las áreas con mayor crecimiento es el análisis de datos, base fundamental para la revolución de la inteligencia artificial. 
Jupyter notebook, Pandas, Numpy, Matplotlib son las librerías básicas y mostraré brevemente su funcionamiento.

Su gran éxito se debe a una increíble combinación de potencia con sencilles de uso, y el hecho de que permite 
obtener resultados que nos dan una interpretación de los datos obtenidos.

Contenido
=========
Introducción a la ciencia de datos
Duración: 2 horas

Se usarán datos públicos que los estudiantes pueden descargar para seguir lo mostrado paso a paso.

- Introducción (15 min): ¿Para qué sirve el análisis de datos? ¿Qué ventajas tiene python en ésta área?
- Jupyter (15 min): ¿Qué es un jupyter notebook? Cómo podemos utilizarlo online con google o instalarlo en nuestras computadoras.
- Pandas (30 min): Se enseñará las bases para importar datos desde un archivo csv, exportarlo, realizar un filtrado, selección y ordenamiento básico.
- Numpy (20 min): Procesamiento de arreglos de números usando numpy. 
  Se mostrará como sacar indicadores que nos permitan tener una idea de los números. 
  También como afrontar el caso de los datos faltantes.
- Matplotlib (20 min): Se mostrará como hacer gráficas sencillas que nos permitan visualizar los datos. Se mostrará gráficas de torta, de línea y de barra
- Conclusión (20 min): Para reflexionar sobre las capacidades del software mostrado, sus usos actuales y otras aplicaciones.


Requerimientos
==============
Los estudiantes precisan tener Jupyter-lab instalado en sus máquinas 
o tener conexión a internet para usar los servicios de Google Colab. 
Para poder seguir el taller paso a paso también se requiere bajar el set de datos.


