.. title: Autopsias informáticas: extendiendo software libre forense
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
Autopsias informáticas: Extendiendo software libre forense con Python

Descripción
===============
Una rápida introducción a la realización de pericias informáticas forenses, dando un breve repaso a las 
tecnologías de software libre disponibles para computadoras. Luego explico cómo realizar plugins del 
Autopsy 4.8, dando una explicación detallada de uno desarrollado por mí y publicado con licencia GPL. 
Orientada a novatos en informática forense, interesados en tener una idea básica del trabajo pericial, 
o bien forenses que puedan ver la oportunidad de conocer más sobre programación de plugins, 
que puedan presentarse en la Open Source Digital Forensics Conference (OSDFCon).

Contenido
=========
Voy a explicar la realización de plugins para el software forense llamado Autopsy SleuthKit, el más utilizado software 
libre para el análisis de dispositivos de almacenamiento.. Es un software realizado en Java que permite la realización 
de plugins en Jython, permitiendo utilizar la API disponible para Java.. Introduzco describiendo el proceso de realizar 
una pericia informática utilizando software libre bajo linux, y software gratuito en windows. Luego paso a explicar los 
desarrollos realizados paso a paso y su utilización práctica
Vamos a analizar los datos extraídos, usando la herramienta líder del software Libre.
Revisaremos velozmente discos completos, buscaremos por palabras claves, archivos conocidos por hash.
Encontraremos archivos escondidos, borrados, ficheros similares, geolocalizados, y analizaremos una linea de tiempo.



Recursos
=============
🎞 : `Video PyConAr 2018 <https://youtu.be/t2yEmbPQlY4>`_

🎞 :  `Video PyCon Charlas 2019 <https://youtu.be/iiIGvoIbCyc>`_

🎞 :  `Video NotPinkConf 2019 <https://youtu.be/fgRr3oUiK_w>`_

✍ : `Presentation PyCon Charlas 2019 <https://pycon-archive.python.org/2019/schedule/presentation/342/>`_

💾: `Diapositivas Autopsias Informáticas </AutopsiasInformáticas.pdf>`_

