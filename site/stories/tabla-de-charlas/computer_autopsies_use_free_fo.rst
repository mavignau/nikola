.. title: Computer Autopsies: Use Free Forensic Software
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
María Andrea Vignau will cover a real world case solved using Autopsy, giving all the necessary introduction to the work.

Description
===============
All the forensic work will be explained step by step, 
using free software Autopsy.
From taking the evidence in the laboratory or crime scene to the analisis.

Outline
=======
1- Get evidence – Considerations about getting evidence from the field, preserving it. Chain of custody.

2- Make forensic copies – Using free software to make forensic sound images on evidence.
Using open source software.

3- Data analysis with Autopsy – The Autopsy SleuthKit’s workflow, from creating a case to obtaining reports.
Overview of the Graphical Interface and the possibilities.

4- Extending Autopsy with Python – How to extend autopsy using python, creating modules.
The developing environment, module’s types and use cases of each one.

5- María’s plugin, used to present evidence in a real case. Her real experience using this extending capabilities included in Autopsy, and why it was very helpful on a specially difficult case.


Resources
=============
🎞 :  `Video OSSDFCom Webinar <https://youtu.be/ocuFZ8RA1p8>`_

💾: `Slides </ComputerAutopsies.pdf>`_
