.. title: The Zen of Python testing: beyond the debugger
.. date: 2024-12-07 23:22:34 UTC+03:00
.. tags: charlas testing
.. category: stories
.. link:
.. description:
.. type: text


Abstract
========
Dive into the essentials of testing and automation to boost software quality and delivery. 
Learn why testing matters, what to automate, types: unit, functional, integration, performance and usability, 
TDD/BDD philosophies, best practices, and tools like pytest. 
Gain practical insights to build reliable, maintainable software faster

Description
===========
Unlocking the Power of Automated Testing: A Beginner's Guide to Writing Better Code"

Join this special talk to kickstart your journey into the world of automated testing! Designed for developers of all experience levels, this session dives into the fundamentals of testing and why automation is an essential part of creating robust, maintainable, and scalable software.

We’ll start with the basics—breaking down core testing concepts and exploring the crucial reasons behind automating your tests. From there, we’ll guide you through the different types of testing, including unit, functional, integration, performance, and usability, showing how each plays a key role in ensuring software quality.

But testing isn’t just about tools—it’s about strategy! You'll gain a deeper understanding of popular testing philosophies like Test-Driven Development (TDD) and Behavior-Driven Development (BDD), and learn how these approaches can bring clarity and focus to your code. Along the way, we’ll discuss best practices and practical concepts, such as mock and patch, that make testing efficient and manageable.

Finally, we’ll introduce powerful libraries like unittest, pytest, and pytest_bdd—tools you can start using immediately to take control of your testing process.

By the end of this talk, you’ll walk away with a solid understanding of automated testing and the tools and strategies needed to bring it into your development workflow. Whether you’re a beginner eager to take your first steps or an experienced developer looking to refine your approach, this session has something valuable for everyone.

Outline  
=================================

In a brief talk format
~~~~~~~~~~~~~~~~~~~~~~~

This is a special talk to initiate the use of automated tests, 

It includes 

- the basic concepts of testing, 

- reasons to automate tests, 

- types of testing: unit, functional, integration, performance, usability. 

- testing philosophies such as test driven design and behavior driven design.

- main libraries for testing: unittest, pytest, pytest_bdd

- concepts of mock, patch

- best practices in testing

In a workshop format
~~~~~~~~~~~~~~~~~~~~~~~
Add an indepth of more used pytest features, including parameters, fixtures.

Add examples of BDD put in practice with a hands-on complete example. 

Discuss mocking and patching basic techniques.


Resources
=============
✍ : `Presentation PyLadies 2024 <https://pretalx.com/pyladiescon-2024/talk/HKYPSL/>`_

💾: `Slides 90 min workshop version </ZenTesting_workshop.pdf>`_
