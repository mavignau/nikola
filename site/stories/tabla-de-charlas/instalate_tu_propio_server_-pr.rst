.. title: Instalate tu propio server -Proxmox de Cero-
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
Instalación de Proxmox y su uso para un ambiente de pruebas y personal.

Descripción
===============
¿Estás interesado en tener tu propio server, en poder probar distintas tecnologías, 
sistemas operativos, en probar la seguridad o la implementacion de software fácilmente?
Proxmox de Cero, con demostraciones y paso a paso, para que con cualquier PC disponible, 
puedas empezar.
Seguimos con la Instalación y configuracion para tener windows virtual y una 
nube personal con Nextcloud fácil y sin esfuerzo.

Contenido
=========
- Descripción de las tecnologías de virtualización
- Instalación de proxmox
- Intalación de una máquina virtual con Windows
- Instalación de un contenedor LxC con una apliance NextCloud

Recursos
=============
🎞:
`Video Autodefensa Digital 2019 <https://youtu.be/r1UBLiPGQ7g>`_

💾: `Diapositivas </Proxmox.pdf>`_




