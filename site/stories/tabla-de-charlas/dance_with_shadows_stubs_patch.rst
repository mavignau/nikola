.. title: Dance with shadows: stubs, patch and mock
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
========
To ensure quality, automated testing is a must. But sometimes is impossible or very expensive to use real environments. In this case, you can isolate some parts of a system and use fake simulated objects.

Description
===========
A comprehensive but simple introduction to the use of fake objects. Explain how to inject this object and use 
in test using patch and the awesome and powerful mock objects . Last, I present some very interesting specialized 
libraries for mocking on web development.

Outline
=======
0:00 I present the key factors to use fake objects, and present some dangers.

3:00 Discuss some wanted characteristics in this kind of components.

6:00 Patching: how to do that and some common mistakes. After that I present patch scopes and some disadvantage in the use of this technique.

10:00 Inverse dependency as an possible alternative to patch

13:00 Mocks properties: return value, side effect and specs. Using mocks as spy functions or wrappers. Asserting on callings.

21:00 Using special libraries for mocking. Presenting pyvcr and moto.

Resources
=============
✍ :  `Presentation Europython 2022 <https://ep2022.europython.eu/session/dance-with-shadows-stubs-patch-and-mock>`_

💾: `Slides </Mocks_en.pdf>`_
