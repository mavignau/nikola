.. title: Autopsy Revelado
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
Taller de Autopsy, software líder en análisis forense de computadoras

Descripción
===============
Vamos a analizar los datos extraídos, usando la herramienta líder del software Libre.
Revisaremos velozmente discos completos, buscaremos por palabras claves, archivos conocidos por hash.
Encontraremos archivos escondidos, borrados, ficheros similares, geolocalizados, y analizaremos una linea de tiempo.

Contenido
=========

- Instalación
- Presentación y conceptos
- Módulos automáticos
- Interfaces especiales
- Etiquetado y reportes

Recursos
=============
🎞:  `Video Autodefensa Digital 2020 <https://youtu.be/BRFHw-ythl8>`_

💾: `Diapositivas Autopsy Revelado </AutopsyRevelado.pdf>`_



