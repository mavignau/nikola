.. title: CertMailer - Automatizar envío de certificados
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
Taller de Autopsy, software líder en análisis forense de computadoras

Descripción
===============
Explico la necesidad, los recursos encontrados, cómo combiné partes y cómo extraigo la información pertinente de EventoL 
¿Qué más falta? Un montón: desafíos, issues, ideas

Contenido
=========
- Motivación
- Uso
- Desafío
- Planes

Recursos
=============
🎞 : `Video PyConAr 2019 <https://youtu.be/WE_EqJAjPAE>`_

✍ :  `Presentacion PyConAr_2019 <https://eventos.python.org.ar/events/pyconar2019/activity/281/>`_

💾: `Video CertMail </CertMail-v2.pdf>`_

