.. title: De 0 a Speaker. 
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas speaker
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
Tips, sugerencias y mucha info para dar charlas

Descripción
===============
Explico porqué y el cómo dar charlas en eventos técnicos. 

Las razones que me llevaron a empezar, los tips para mejorar nuestras posibilidades de selección, 
cómo dar demos, preparar diapositivas, y hasta cómo aprender a hacerlas en otros idiomas.

Contenido
=========
- Motivación: razones por las que creo es muy valioso hacer charlas
- Elegir tema: ¿cómo elegir el tema?
- Investigar: herramientas y fuentes para realizar la investición de base
- Diseñar: tips para lograr diapositivas geniales
- Exponer: cuidados en el momento de la exposición

En conclusión, ser speaker es una gran experiencia y me ha ayudado a conocer personas, tecnologías y lugares.


Recursos
=============
🎞 : `Video https://youtu.be/T94G9KaAVKw<>`_

💾: `Diapositivas De 0 a Speaker </De0aSpeaker.pdf>`_

