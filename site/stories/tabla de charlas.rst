.. title: Charlas brindadas
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text

En inglés
=========
- `The Python Zen of Testing: beyond debugger <testing_zen_eng/>`_ 
- `Dance with shadows: stubs, patch and mock <dance_with_shadows_stubs_patch/>`_ 
- `Computer Autopsies: Use Free Forensic Software <computer_autopsies_use_free_fo/>`_ 
- `SQLite, an (un) known super ant <sqlite_an_un_known_super_ant/>`_ 


Técnicas:
=========

Python
^^^^^^^^^^^^^^
- `El Zen del Testing en Python: mas allá del debugger <testing_zen_es/>`_ 
- `IA sin misterios: entre datos, algoritmos y redes <ia_sin_misterios/>`_ 
- `Haz ORO tus datos con SQLAlchemy II <haz_oro_tus_datos_sqlalchemy/>`_ 
- `Introducción a Python con Jupyter Lab <introduccin_a_python_con_jupyt/>`_ 
- `Bailo con tu sombra: patch, mock y stub. <bailo_con_tu_sombra_patch_mock/>`_ 
- `Python y Documentos: una fructífera relación <python_y_documentos_una_fructf/>`_ 
- `SQLite, la (des)conocida Super Hormiga <sqlite_la_desconocida_super_ho/>`_ 

Forenses
^^^^^^^^^^^^^^
- `Autopsias informáticas: extendiendo software libre forense <autopsias_informticas_extendie/>`_ 
- `Celular, un testigo posible y silencioso <celular_un_testigo_posible_y_s/>`_ 
- `Autopsy Revelado <autopsy_revelado/>`_ 

Otros 
^^^^^^^^^^^^^^
- `Terminalt Utopía <terminaltopia/>`_ 
- `Instalate tu propio server -Proxmox de Cero- <instalate_tu_propio_server_-pr/>`_ 
- `CertMailer - Automatizar envío de certificados <certmailer_-_automatizar_envo_/>`_ 

Generales
==========
- `De 0 a Speaker. Cómo dar charlas <dar_charlas/>`_ 
- `Porque hacemos Soft Libre <porque_hacemos_soft_libre/>`_ 
- `Triunfar con Python: Vivencias y reflexiones de una programadora. <triunfar_con_python_vivencias_/>`_ 

Mis redes sociales
==================
- `Linkedin @mavignau <https://www.linkedin.com/in/mavignau/>`_
- `Github marian-vignau <https://github.com/marian-vignau>`_
- `Gitlab mavignau <https://gitlab.com/mavignau>`_
- `Telegram mavignau <https://t.me/mavignau>`_
- `Mastodon @mavignau@social.linux.pizza <https://social.linux.pizza/@mavignau>`_
- `Facebook mavignau <https://www.facebook.com/mavignau/>`_
- `Instagram mavignau <https://www.instagram.com/mavignau/>`_
- `Twitter mavignau <https://twitter.com/mavignau>`_

