.. title: El Software y yo - Un curriculum informal
.. date: 2018-07-10 23:22:34 UTC+02:00
.. tags: curriculum, referencias
.. category: blog
.. link:
.. description:
.. type: text

El Software y yo
================

Un curriculum informal
-----------------------

Mi mamá era programadora en la primera computadora de la Provincia del
Chaco cuando se embarazó de mí [0]_. Mi papá aprendió un poco después con su
calculadra Texas Instruments TI59, que programaba con ella. Y a mí me
mandaron a aprender a programar a los 10 años, y ¡me encantó!. Terminé
una de las mejores programadoras infantiles.

Tokens, algoritmos, recursividad, pixels, direcciones de memoria,
sprites, códigos de caracteres, Logos, Basic, Lisp eran las palabras del
lenguaje secreto de mi adolescencia, que compartía sólo con la familia,
jamás con amigos. Salía del aburrimiento del colegio secundario y
programaba, y fuí una ávida lectora de ciencia ficción, clásicos,
historias románticas, enciclopedias y todo lo que se viniera a mis
manos. Hasta los libros de matemáticas universitarias y sexualidad de
mamá.

Con mi papá sacábamos todo el provecho a nuestra Commodore 128, con
diskettera e impresora de matriz de puntos MP1000 a color, eventualmente
hasta tuvimos un pequeño plotter. Mi mamá optimizaba el código de mi
padre, lo ayudaba con las matemáticas más avanzadas, (él era ingeniero
estructuralista, pero mi madre fué titular de Análisis Matemático e
investigadora en Matemática) A mí me tocaba la parte gráfica, diagramas
de corte, esfuerzo, y de cargas. Estudié Ingeniería en Sistemas en la
UTN, y ahí conocí y me puse de novia con el que sería mi marido.
Pertenecí a Fidonet, cuando éramos sólo tres en el Chaco, y llegamos a
organizar una de las primeras reuniones nerd del norte del país. En
aquel histórico FidoAsado, era la única chica.

Antes de recibirme, me contrataron en ECOM, la empresa informática del
gobierno de la Provincia del Chaco, donde mi madre era conocida por los
más viejos empleados, algunos de ellos compañeros de trabajo en su
tiempo. Allí trabajé programando durante 7 años, primero en VisualBasic,
posteriormente también para mainframe en Natural/Adabas. Después fuí
capacitada en Genexus, una herramienta que nunca consideré que obtuviera
buenos resultados. Terminé de estudiar inglés en Extensión Universitaria
de la UNNE, y rendí el First Examination de la Universidad de Oxford.

Siempre me pareció genial poder acceder al código fuente de los
programas. Mi primer intento de instalación de linux fué con slackware,
cuando todavía había que pasar las imágenes a los diskettes. Mi primer
instalación con éxito, cuando por fin tuve una pc propia y tuve Red Hat
5, luego Turbolinux, Mandrake, un montón!. Y allá por el año 2004 empezó
a llamarme la atención python: un lenguaje que estaba hecho para ayudar
al programador. “Dive into Python” es todavía uno de los mejores libros
técnicos que leí.

Salí de ECOM hastiada de la poca innovación en la empresa, y ya
embarazada, me dieron la oportunidad para trabajar en el Poder Judicial
del Chaco. Privilegié la estabilidad laboral: no había espacio para
arriesgarme trabajando en una startup con un bebé recién nacido. Fué un
largo tiempo en el que la programación dejó de ser profesión y se
convirtió en hobby. Durante las horas laborales, aprendí a sacar
información del sistema LexDoctor usando python, decidida a usar
software libre en el juzgado en el que trabajaba. Gracias a él hice
estadísticas, y me auxilié en varias tareas que me asignaron. El tiempo
pasaba entre criar a mis niñas pequeñas y tener instantes para
concentrarme en el software que poco a poco elaboraba. Hoy es
pylexcom [1]_, una biblioteca de código abierto para línea de comandos.

Posteriormente, se realizó una profunda reestructuración en mi lugar de
trabajo. Así que realicé trámites hasta que conseguí
un traslado para colaborar con un colega que había conocido en un
congreso informático. Lo que no sabía era que me encontraría en uno de
los ambientes más hostiles de mi vida
laboral, con un jefe ex-militar, doctor en abogacía y retrógrado.
Durante ésos años tomé denuncias penales, una experiencia difícil pero
enriquecedora ya que me permitió conocer historias y personas ajenas
completamente a mi cotidianeidad. Técnicamente, de todas formas me
arreglé para realizar BuscaLex [2]_, un programa que indexaba los datos
extraídos del LexDoctor, usando pylexcom y wxPython, que a pesar de
funcionar perfectamente, jamás fué implementado. Ahí fué que decidí
liberar el código de la mayor parte de lo que hacía.

En el año 2008 decidimos con mi hermano menor, economista, iniciar un
emprendimiento de desarrollo de software. Logramos conseguir el subsidio
fonsoft, por la excelente presentación del plan de negocios. Sin
embargo, no tuvimos éxito con el emprendimiento ya que las condiciones
de mercado cambiaron.

Al siguiente año, 2009, viajé al primer Pycamp. El ambiente creativo,
alegre y libre me fascinó, quería traerlo a mi vida diaria.

En el Poder Judicial, los terribles inconvenientes arrastrados llevaron
a realizar una reestructuración completa en el Ministerio Fiscal, así
que fuí elegida para formar parte de un nuevo equipo de trabajo. Durante
mi permanencia en el cargo haciendo repetidas cédulas y oficios, hice el
sistema ATC, ad-hoc y realizado gradualmente, para no perder oficio y
para soportar el tedio, y lo repetitivo de la tarea.

Más tarde, en el año 2014, volvimos a intentar trabajar con mi hermano
incorporando tecnología a una empresa chaqueña de reciclado de plástico
llamada ReciNEA. El software de manejo integral de la organización, que
incluía módulos de fabricación, fué realizado íntegramente por mí
utilizando wxpython y sqlalchemy. Desacuerdos con la dirección
de la empresa familiar impidieron su implementación.

En el año 2015 decidí poner dinero en la pasión, y hice la Diplomatura
en Software Libre con Mariano Reingart. Cada mes viajaba a La Plata,
hice un total de 6 viajes de casi 1000 km, hasta que concluí con uno de
los dos mejores trabajos prácticos finales, un software para estudios
jurídicos llamado OpenLex [3]_.

En 2015, además, trabajé confeccionando sola un software para la
consulta de padrones electorales en los lugares de votación,
el cual fué realizado con wxPython y SQLite y
pudo ser implementado en todos los lugares de votación de la provincia.
Hice una versión web del mismo software de consulta.

En el pycamp del 2016, pude participar de la creación de software en el
lugar, haciendo un pequeño juego llamado Tower Defense [4]_. ¡Todavía me
acuerdo la cara de Facundo viendo la cantidad de gente que se anotó para
trabajar en equipo!. Empezamos a trabajar en la interfaz gráfica con
Tony Abdala, pero después continué sola. Me puse manos a la obra,
resolviendo la parte gráfica, el encargo me traía recuerdos de cuando
era adolescente. El juego, lógicamente, funcionó.

Empecé a hacerme habitué de los pycamp. En el año 2016 Facu anunció por
Twitter su software Recordium, y como había sido una delicia trabajar
con él, decidí probar colaborar a distancia, a ver si podía poner en
práctica lo aprendido durante la Diplomatura y hacer algo útil, más “en
serio”. Los proyectos grandes me intimidaban, y además ¿cuál elegir?
¿alguno que aprecie mi trabajo?. Avanzamos, le agregué algunas features,
preparé un instalador para Windows, hasta que entre obligaciones de ama
de casa y trabajo, no tuve más tiempo para el hobby.

En el Poder Judicial, tuve un desacuerdo con mi jefe inmediato, que
escaló hasta provocar un sumario: el detallado registro informático
obtenido de ése tiempo me sirvió para tener material probatorio logrando
que fuera desestimado. Luego de este incidente, al fin conseguí empezar
a hacer un trabajo técnico nuevamente, ahora como perito forense. Desde
el año 2016 soy la tercer perito especializada en informática en la
Provincia del Chaco. En ése mismo año, terminé la Diplomatura en
Pericias Judiciales.

Ése año volví a acercame para trabajar en Linkode, donde trabajé otra
vez con Facundo y con Mati Barrientos. Encontré una issue que parecía
más fácil para ingresar al proyecto, allá por el año 2016, y durante el
año la completé [5]_.

En el año 2017 me encontré con un colega que quería organizar la Flisol
en Resistencia, pero estaba algo decepcionado: necesitaban gente nueva,
con ganas, entusiasta. Así que logré ponerlos en contacto con el Centro
Cultural Alternativo en Chaco, y pudimos relanzar la Flisol en
Resistencia. En la edición de este año 2018 ya logramos que viniera muchísima más gente,
y estamos juntándonos los organizadores para realizar otro evento en
septiembre. Este año hicimos un encuentro de Python en el NEA, estamos
empezando a formar comunidad.

Facu entonces a mediados de 2017 decidió relanzar su blog, y contratarme
para hacerlo con Nikola. Como siempre me había divertido mucho trabajar
con él, le presupuesté ridículamente bajo, me duplicó lo precios, y
empezamos. Luego de superar varios inconvenientes, en marzo de 2018 se
implementó el sitio web. [6]_

Hoy estoy estudiando Abogacía en la Universidad Austral, preparé una
charla inspirada en las innovaciones realizadas con python en mi trabajo
como forense, y la presenté en la conferencia de seguridad informática
ParanaConf y en el PyDayNEA. [7]_

.. [0]
   https://www.ivoox.com/robotec-programa-21-audios-mp3_rf_25701675_1.html

.. [1]
   https://github.com/marian-vignau/pylexcom

.. [2]
   https://github.com/marian-vignau/buscalex

.. [3]
   https://github.com/UniversidadDelEste/OpenLex

.. [4]
   https://github.com/PyAr/towerdefense-pycamp16/pull/40

.. [5]
   https://github.com/facundobatista/kilink/issues/21

.. [6]
   https://blog.taniquetil.com.ar/posts/0758/

.. [7]
   https://youtu.be/OFbdHaTReyU
