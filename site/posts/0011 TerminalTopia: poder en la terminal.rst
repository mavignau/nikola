.. title: Terminal topía
.. date: 2022-04-21 19:22:34 UTC-03:00
.. tags: charla, terminal, consola, tmux, vim
.. category: blog
.. link:
.. description:
.. type: text



Repositorio con scripts de instalación para UBUNTU
====================================================

- `Terminal Topia <https://gitlab.com/mavignau/terminaltopia>`_

Links a las bibliotecas mencionadas
=====================================


Tmux
^^^^^^
- `Tmux <https://www.hostinger.es/tutoriales/usar^tmux^cheat^sheet>`_
- `Tmuxp <https://tmuxp.git-pull.com/index.html>`_

NeoVim
^^^^^^
- `NeoVim <https://neovim.io/>`_
- `Config Python <https://vim.fisadev.com/>`_

Fish Shell
^^^^^^^^^^^^^^^^^^
- `Playground <https://rootnroll.com/d/fish^shell/>`_
- `Fisher <https://github.com/jorgebucaran/fisher>`_
- `Ssh agent <https://github.com/danhper/fish^ssh^agent>`_
- `Z <https://github.com/jethrokuan/z>`_

Ls Deluxe y Bat
^^^^^^^^^^^^^^^^^^
- `Ls Deluxe <https://github.com/Peltoche/lsd>`_
- `Bat: Cat with Wings <https://github.com/sharkdp/bat>`_

NerdFonts
^^^^^^^^^^^^^^^^^^
- `Readme del repo <https://github.com/ryanoasis/nerd^fonts/blob/master/readme_es.md>`_
- `Site <https://www.nerdfonts.com/>`_

Git
^^^^^^^^^^^^^^^^^^
- `lazygit <https://github.com/jesseduffield/lazygit>`_
- `deltagit <https://github.com/dandavison/delta>`_


Other
^^^^^^^^^^^^^^^^^^
- `LookAtMe <https://pypi.org/project/lookatme/>`_
- `LazyDocker <https://github.com/jesseduffield/lazydocker>`_


