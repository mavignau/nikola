.. title: PyDay 2022 Resistencia Chaco
.. date: 2022-10-21 9:22:34 UTC-03:00
.. tags: evento, propuestas 
.. category: blog
.. link:
.. description:
.. type: text

Se viene un nuevo `PyDay en Resistencia Chaco <https://eventos.python.org.ar/events/PyDayNEA2022/>`_

.. figure:: /galleries/PyDayNEA2022/Banner.png
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Banner del PyDayNE 2022

El nuevo pyday será en la sede de ECOM en día 26 de noviembre.


.. figure:: /galleries/PyDayNEA2022/banner_CFP.png
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Llamado a propuestas
        

Más información en:
~~~~~~~~~~~~~~~~~~~

`Llamado a propuestas <https://eventos.python.org.ar/events/PyDayNEA2022/activity/proposal/>`_ 

`Anotarse para asistir <https://eventos.python.org.ar/events/PyDayNEA2022/registration>`_ 

`Seguirnos en twitter <https://twitter.com/PythonNEA>`_ 

`Unirte a nuestro grupo Telegram <https://t.me/pydaynea>`_ 




