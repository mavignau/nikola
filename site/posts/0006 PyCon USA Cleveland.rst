.. title: PyCon Cleveland, Ohio
.. date: 2019-05-20 09:22:34 UTC+03:00
.. tags: pycon, pyconcharlas
.. category: blog
.. link:
.. description:
.. type: text

`Galería de fotos </galleries/PyConUSA2019/index.html>`_


El día 29 de mayo salí en colectivo desde Resistencia Chaco hacia Buenos Aires, hice un día de escala
y el primero de mayo partí en un vuelo con dos escalas hacia Cleveland, Estados Unidos.

Justo antes de embarcar en el avión a Cleveland, me encuentro con `Cris Ewing <https://twitter.com/crisewing>`_ cuya remera decía
PyLadies, me acerqué a hablar y justamente íbamos al mismo destino.

.. figure:: /galleries/PyConUSA2019/cleveland.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Vista de Cleveland

   Vista de Cleveland

Al llegar, tomé el bus para ir al centro de la ciudad, cerca del cual quedaba en centro de convenciones.
Era plena primavera, así que habían arreglos de flores por todas partes, particularmente, muchos
tulipanes.

.. figure:: /galleries/PyConUSA2019/cleveland\ tulipanes.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Bellísimos tulipanes multicolores

   Bellísimos tulipanes multicolores

Llegué allá al mediodía del dos de mayo, para asistir a la tarde del último día de los tutoriales, y ver
la exposición con los stands. Era muy grande, al menos cinco filas de stands, con destacados de las
mayores corporaciones, otros de empresas menos conocidas, pero importantes,
y con varios stands dedicados a comunidades como PyLadies o DjangoGirls.

.. figure:: /galleries/PyConUSA2019/django\ girls.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: En el Stand de Django Girls

   En el Stand de Django Girls

Nos daban un desayuno, con café, té, varios tipos de leche (común, de soja, de almendras, etc),
avena caliente, pastelería, frutas y algo salado, como burritos. El almuerzo, había una carne,
que podía reemplazarse con tofu, con diversos acompañamientos, como ensaladas, y también,
independientemente, una bandejita de ensalada, que traía algún sandwich, un paquete de
papas fritas (las que nos sirvieron eran más crocantes y duritas, y mucho más sequitas que
las habituales por acá).

Todo el día había café, té y leche, a la tarde se servía una merienda con pastelería y cerraba
a las 7 de la noche cada día.

Al llegar el día 3 de mayo, fuí directamente al salón destinado a la PyCon Charlas.
Como llegué temprano, me encontré hablando con `Naomi Ceder <https://twitter.com/NaomiCeder>`_
, que estaba esperando a las personas

.. figure:: /galleries/PyConUSA2019/Naomi\ Ceder.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Junto a Naomi Ceder

   Junto a Naomi Ceder

Mi charla era la segunda programada en la mañana, así que esperé hasta que me tocó darla

.. figure:: /galleries/PyConUSA2019/Charla\ inicio.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Iniciando mi charla

   Iniciando mi charla

Durantes los tres siguientes días había actividades diversas para hacer: se podía asistir a las charlas,
se podía ir a los open spaces, salones que podían ser solicitados por personas o grupos para realizar actividades
diversas, cada una a diferente hora.

.. figure:: /galleries/PyConUSA2019/open\ spaces.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Tablero de open spaces, donde los asistentes proponían temas

   Tablero de open spaces, donde los asistentes proponían temas

Los últimos días fueron para los sprints, los cuales dan una oportunidad única
de trabajar en proyectos de fuente abierta junto a los más reconocidos
desarrolladores de software. Primero ingresé a trabajar al sprint de Flask,
un framework para desarrollo web con un enfoque modular, muy utilizado
por su sencillez.

.. figure:: /galleries/PyConUSA2019/sprints.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Los proyectos en los que se podía realizar desarrollo

   Los proyectos en los que se podía realizar desarrollo


Más tarde me encontré con
`Marlene Marchena‏ <https://twitter.com/msmbru13>`_, una mujer peruana que venía a dar
un taller sobre iniciación en el desarrollo para niños, utilizando
Python para programar sobre Minecraft.

Ella me llevó al sprint sobre BeeWare, un ambicioso proyecto que pretende
ser un completo framework para desarrollo multiplataforma, y como
`Russell Keith Magee <https://twitter.com/freakboy3742>`_,
el principal desarrollador, tenía simplificado el
accesos para los principiantes, durante el segundo día y tercer día.
A cada persona que lograba hacer una contribución al proyecto, nos
daba una pequeña medalla, ambas logramos tener nuestro pequeño trofeo.

.. figure:: /galleries/PyConUSA2019/Russell\ Keith\ Magee.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Russell Keith Magee daba un medalla por la primera contribución.

   Russell Keith Magee daba un medalla por la primera contribución.

Durante el almuerzo del tercer dia de sprints, armamos una mesa en la
de hispanohablantes, y me enteré que se estaba realizando la traducción
al castellano de parte de la documentación de Python, y que la persona
a cargo era el español `Raúl Cumplido <https://twitter.com/raulcumplido>`_
, compañero de trabajo de `Manuel Kauffmann <https://twitter.com/reydelhumo>`_.

.. figure:: /galleries/PyConUSA2019/Raul\ Cumplido.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Con Raúl Cumplido.

   Con Raúl Cumplido.

Los últimos dos días tuve algo de tiempo para conocer la ciudad de Cleveland.
Tiene una arquitectura magnífica, pude disfrutar del Rock & Roll Hall of Fame,
de la Biblioteca Pública, del Centro Glenn Close de la NASA, y del Museo de Arte.
También ví la Universidad Estatal de Cleveland, son dos manzanas con dos torres
de más de 20 pisos y varios otros edificios de al menos 5 pisos cada uno.


.. figure:: /galleries/PyConUSA2019/cleveland\ horizonte.jpg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Paisaje de Cleveland.

   Paisaje de Cleveland.


Para ver muchas más fotos, en la `galería </galleries/PyConUSA2019/index.html>`_







