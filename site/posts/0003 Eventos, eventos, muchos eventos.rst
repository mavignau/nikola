.. title: Eventos, eventos muchos eventos
.. date: 2019-02-22 23:22:34 UTC+02:00
.. tags: pycamp, pyconUS, flisol
.. category: blog
.. link:
.. description:
.. type: text

Armando varios eventos al mismo tiempo, y preparada para viajar a otros tantos

PyCamp
=========

Por fin llegamos al pycamp 2019, voy a viajar con mi hija y con un amigo de muchos
años, Ariel @panflin Nardelli. Estoy anticipando ansiosa la salida, esperando
divertirme un montón como otros años.
Todos los detalles ya están en su lugar. ¡Mucha expectativa!

PyCon US
=========

Voy a viajar este año a la PyCon de Cleveland, Ohio. Me ayuda con los gastos la
PSF, y estoy verdaderamente ilusionada con conocer por primera vez EEUU.
Los detalles del trámite de la visa, los pagos del vuelo, y todo el papeleo, 
me abruman un poco, me parece sin embargo una excelente oportunidad de dar
a conocer las investigaciones forenses y los desarrollos que hacemos en la 
provincia.-

Flisol
=========

Como todos los años, desde el 2017, vengo coorganizando junto a Rolando @pelin
Rodriguez la Flisol, este año lo hacemos en el Informatorio, A. Illia 1055
Estamos saliendo con el llamado a charlas y propuestas de trabajo, 
esperamos que continúe creciendo el evento como lo viene haciendo.
Mi charla va a introducir a los multimedia con soft libre, dando un ejemplo
práctico de cómo hacer un pequeño video tutorial. 

PyDay NEA
=========

Finalmente, también vamos a hacer PyDay NEA junto a Sergio Lapertosa, otro capo.
Esta vez será el 8 de junio, en la UTN Facultad Regional Resistencia, vamos 
a usar el Salón de Actos y el playón para éso. Todavía no tengo decidido el tema
espero contar con el apoyo de algunos genios para traer mucha más gente todavia.

