.. title: OpenLex en Google Summer of Code 
.. date: 2021-03-21 03:22:34 UTC+03:00
.. tags: openlex, google, summer of code, gsoc
.. category: blog
.. link:
.. description:
.. type: text


Este año soy mentora del Google Summer of Code, por segundo año consecutivo.

¿Qué es el Google Summer of Code?
---------------------------------

Es un programa global organizado por Google que acerca a los estudiantes
al software libre, a las organizaciones de software libre relacionadas a
la tecnología para ser parte de las comunidades, y de paso, tener un
sueldo.

Los estudiantes van a tener la posibilidad de trabajar como en realidad
se hace, aplicando los conocimientos teóricos, trabajando en grupo en
forma remota o presencial, utilizando herramientas indispensables como
control de versiones, github, etc y usando los estándares actuales en
desarrollo de software. ¡Y ganando una beca por ello!

Todo esto tutelado/guiado por mí y probablemente por alguien más, en un
ambiente amigable y seguro, y en español.

El proyecto es OpenLex, un software para web diseñado y desarrollado por
mí, pensado para el manejo de estudios de abogacía, que necesita amor
para crecer, y así ser modernizado y completado:

-  Migrarlo a Python3, y a Py4Web
-  Actualizar las versiones de las librerías JavaScript
-  Preparar una versión demo on line.
-  Armar un manejo de usuarios más robusto
-  Algunas features adicionales son muy necesarias, como implementar el
   uso de templates para los escritos.

 Para ver más, podrías mirar en

{{% media url="https://youtu.be/GK1-XE2Nxdc" %}}

¿Qué se precisa?
----------------

-  compromiso real,
-  conectividad estable y disponible para una o dos reuniones semanales
   on line (si tu conexión es lenta, por lo menos se necesita
   transmisión de audio).
-  una computadora a la que tengas acceso 18 hs por semana como mínimo.
-  instalarte Telegram, porque ése va a ser nuestro medio de
   comunicación habitual, ahí tenemos coordinar reuniones y comunicarnos
   día a día.
-  Ser apto para GSoC: tener más de 18 años, estar estudiando algo
   relacionado a la informática posterior al secundario o haberse
   recibido entre 1/12/20 y 17/05/21, y poder trabajar.

En lo técnico, lo que necesitás es muchas ganas, pero estaría bueno si
conocés python, git, javascript, algo sobre programación web. ¿No
conocés todo esto? ¡Esto es un proceso de aprendizaje, vamos todos
juntos a apoyarnos en el desafío! Abajo agrego una lista de algunos
sitios fantásticos donde aprender todo lo necesario.

Entonces, ¿tenés ganas de participar? ¿Qué hacer?
-------------------------------------------------

#. Creá tu plan de proyecto: hay que investigar un poco, pensar posibles
   soluciones alternativas, inspirándose en la lista de issues.
#. Acordate que GSoC se maneja en fases, la primera fase es para
   conocerse en la comunidad y familiarizarse con el código y su estilo,
   después viene la fase inicial de codificación, en l a cual serás
   evaluade por mí, como mentora,  y luego vendrán las 5 semanas de
   codificación, al final de ellas, tendrás que entregar algún producto
   y dar el URL.
#. Pagos: los pagos son en dos partes , la primera luego de pasar la
   fase 1, y la última luego de terminar, utilizándose la plataforma
   Payoneer, un total de 1500 U$S. Cuando seas aceptade, el link para
   registrarte llegará a tu email. Tendrás un certificado de
   finalización al completar el programa.

Las fechas límite:
------------------

29/03/2021 al 13/04/2021: Para que les alumnes presenten sus propuestas.

13/04/2021 al 17/05/2021: Para que revisemos las propuestas presentadas

17/05/2021 al 07/06/2021: Me presento con les estudiantes como mentora, y
en este tiempo pueden aprender más sobre la organización y el código
existente.

07/06/2021 al 16/08/2021: Codear, codear, codear!

12/07/2021: Les estudiantes y yo nos evaluamos mutuamente.

16/08/2021: Les estudiantes envían su código, la síntesis de su proyecto
y las evaluaciones finales de mentoría.

23/08/2021: Evalúo el código de les alumnes y dictamino si se cumplieron
los objetivos del proyecto.

31/08/2021: Les alumnes reciben la notificación, y si pasaron o no los
proyectos.

¿Te gustó?¿Todavía tenés dudas?
-------------------------------

Bueno, podés leer más sobre GSoC:

El sitio oficial
`summerofcode <https://summerofcode.withgoogle.com/>`__
,

El manual del alumno
`student guide <https://google.github.io/gsocguides/student/index>`__
 

Sobre el pago:
`payoneer <https://developers.google.com/open-source/gsoc/help/payoneer>`__


Y sobre las tecnologías que empleamos:

Python3: 
`El tutorial de Python <https://docs.python.org/es/3/tutorial/index.html>`__
`Inmersión en Python <https://argentinaenpython.com/quiero-aprender-python/inmersion-en-python-3.0.11.pdf>`__

Py4Web:
`py4web documentation <https://py4web.com/_documentation/static/en/index.html>`__

JavaScript:
`w3school javascript <https://www.w3schools.com/js/DEFAULT.asp>`__

GIT:
`gitexplorer <https://gitexplorer.com/>`__
`try github <https://try.github.io/>`__
 
